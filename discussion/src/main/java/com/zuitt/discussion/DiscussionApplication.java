package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@SpringBootApplication
// This Application will function as an endpoint that will be used in handling http request.
@RestController
@RequestMapping("/greeting")
public class DiscussionApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}

//	localhost:8080/hello
	@GetMapping("/hello")
//	Maps a get request to the route "/hello" and invokes the method hello().
	public String hello(){
		return "Hello World";
	}

//	Route with String Query
//	localhost:8080/hi?name=value
	@GetMapping("/hi")
//	"@RequestParam"	annotation that allows us to extract data from query strings in the URL
	public String hi(@RequestParam(value="name", defaultValue = "John") String name){
		return  String.format("Hi %s", name);
	}

//	Multiple parameters
//	localhost:8080/friend?name=value&friend=value
	@GetMapping("/friend")
	public String friend(@RequestParam(value = "name", defaultValue = "Joe") String name, @RequestParam(value = "friend", defaultValue = "Jane")String friend){
		return String.format("Hello %s! My name is %s.", friend, name);
	}

//	Route with path variables
//	Dynamic data is obtained directly from the url
//	localhost:8080/{name}
	@GetMapping("/hello/{name}")
//	"@PathVariable" annotation allows us to extract data directly from the URL
	public String greetFriend(@PathVariable("name") String name){
		return String.format("Nice to meet you %s!", name);
	}

//	Activity S09 Starts HERE!

//	Create ArrayList
	ArrayList<String> list = new ArrayList<String>();
// Route for Enroll
	@GetMapping("/enroll")
	public String enroll(@RequestParam(value = "user") String user){
		if(list.add(user)){
			return String.format("Thank you for enrolling, %s!", user);
		}else {
			return String.format("UnSuccessful Enrollment!");
		}
	}
// Get Enrollees from the list array
	@GetMapping("/getEnrollees")
	public String enrollees(){
		return list.toString();
	}

	@GetMapping("/nameage")
	public String nameAge(@RequestParam(value = "name") String name, @RequestParam(value = "age") Integer age){
		return String.format("Hello %s! My age is %d.", name, age);
	}

	@GetMapping("/courses/{id}")
	public String courseId(@PathVariable("id") String courseId){
		if(courseId.equals("java101")){
			return "Java 101, MWF 8:00AM-11:00AM, PHP 3000.00";
		}else if (courseId.equals("sql101")){
			return "SQL, TTH 1:00PM-4:00PM, PHP 2000.00";
		}else if(courseId.equals("javaee101")){
			return "Java EE 101, MWF 1:00PM-4:00PM, PHP 3500.00";
		}else{
			return "Course is not Found!";
		}
	}

//	Activity Asynch CIT-S09
	@GetMapping("/welcome")
	public String user(@RequestParam(value = "user") String user, @RequestParam(value = "role") String role){
		if(role.equals("admin")){
			return String.format("Welcome back to the class portal, Admin %s!", user);
		}else if(role.equals("teacher")){
			return String.format("Thank you for logging in, Teacher %s!", user);
		}else if(role.equals("student")){
			return String.format("Welcome to the class portal, %s!", user);
		}else{
			return "Role out of range!";
		}
	}

	ArrayList<Student> StudentList = new ArrayList<Student>();
	@GetMapping("/register")
	public String user(@RequestParam(value = "id") int id, @RequestParam(value = "name") String name, @RequestParam(value = "course") String course){
		Student st = new Student(id, name, course);
		StudentList.add(st);
		return String.format("%d your id number is registered on the system!", st.getId());
	}
	@GetMapping("/account/{id}")
	public String account(@PathVariable("id") int id){
		Student st2 = new Student();
		int accountId = 0;
		if(StudentList.size() > 0) {
			for (Student student : StudentList) {
				accountId = student.getId();
				if (accountId == id) {
					st2 = student;
				}
			}
		}
		if(st2.getId() == id && st2.getName() != null && st2.getCourse() != null){
			return String.format("Welcome back %s! You are currently enrolled in %s", st2.getName(), st2.getCourse());
		}else{
			return String.format("Your provided %d is not found in the system!", id);
		}
	}
}
class Student{
	private int id = 0;
	private String name = null;
	private String course = null;

	Student(){}
	Student(int id, String name, String course){
		this.id = id;
		this.name = name;
		this.course = course;
	}

	public void setID(int id){
		this.id = id;
	}

	public void setName(String name){
		this.name = name;
	}

	public void setCourse(String course){
		this.course = course;
	}

	public int getId(){
		return this.id;
	}

	public String getName(){
		return this.name;
	}

	public String getCourse(){
		return this.course;
	}

}